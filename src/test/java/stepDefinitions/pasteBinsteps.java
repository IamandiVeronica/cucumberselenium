package stepDefinitions;

import io.cucumber.java.en.*;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;


public class pasteBinsteps {
    ChromeDriver driver;
    @Given("User launch chrome browser")
    public void user_launch_chrome_browser() {
            System.setProperty("webdriver.chrome.driver", "/Users/Veronica/Downloads/chrome/chromedriver.exe");
        driver=new ChromeDriver();
    }

    @When("User navigate to https:pastebin.com")
    public void user_navigate_to_https_pastebin_com() {
        driver.get("https://pastebin.com/");

    }

    @Then("User enter a new paste in New Paste field")
    public void user_enter_a_new_paste_in_new_paste_field() {
        driver.findElement(By.id(("postform-text"))).sendKeys("<?php echo 'testing' ?>");

    }

    @When("User click submit button Create New Paste")
    public void user_click_submit_button_create_new_paste() {
        driver.findElement(By.className("btn")).submit();

    }


    @And("close browser")
    public void close_browser() {
        driver.quit();

    }

}
