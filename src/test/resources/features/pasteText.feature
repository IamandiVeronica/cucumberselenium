Feature: Test the pasting functionality at the https:pastebin.com

  Scenario: Create a new paste
    Given User launch chrome browser
    When User navigate to https:pastebin.com
    Then User enter a new paste in New Paste field
    When User click submit button Create New Paste
    And close browser
